-- Create user for Propiedades App
CREATE USER `propiedades`@`localhost` IDENTIFIED BY 'araujo';

GRANT ALL PRIVILEGES ON Testing_fullstack.* TO `propiedades`@`localhost`;
FLUSH PRIVILEGES;
